# load the digits dataset from scikit-learn
# 901 samples, about 180 samples per class
# the digits represented 0,1,2,3
from sklearn import datasets
from pylab import text,show,cm,axis,figure,subplot,imshow,zeros
from minisom import MiniSom
import time

digits = datasets.load_digits(n_class=3)
data = digits.data # matrix where each row is a vector that represent a digit.
num = digits.target # num[i] is the digit represented by data[i]
print digits.data[1]
print num
classes = [0, 0, 0]
for x in num:
	classes[x] = classes[x] + 1
print classes
# training the som
som = MiniSom(20,20,64,sigma=.8,learning_rate=0.5)


#make a loop for 10 iterations of SOM
for i in range(5):
	print("Training...")
	start = time.time()
	som.train_batch(data,num,10) # random training
	end = time.time()
	print end - start
	print("\n...ready!")

	# plotting the labels
	wmap = {}
	figure(i)
	im = 0
	for x,t in zip(data,num):
		w = som.winner(x)
		wmap[w] = im
		text(w[0]+.5, w[1]+.5, str(t), color=cm.Dark2(t / 4.), fontdict={'weight': 'bold', 'size': 11})
		im = im + 1
	axis([0,som.weights.shape[0],0,som.weights.shape[1]])

	#count classes instances after organizing map
	classes = [0, 0, 0]
	for x in num:
		classes[x] = classes[x] + 1
	print classes
#second figure - digits as numbers
#figure(2,facecolor='white')
#cnt = 0
#for j in reversed(range(20)): # images mosaic
#	for i in range(20):
#		subplot(20,20,cnt+1,frameon=True, xticks=[], yticks=[])
#		if (i,j) in wmap:
#			imshow(digits.images[wmap[(i,j)]], cmap='Greys')
#			#print digits.images[wmap[(i,j)]]
#		else:
#			imshow(zeros((8,8)), cmap='Greys')
#		cnt = cnt + 1

show() # show the figure
