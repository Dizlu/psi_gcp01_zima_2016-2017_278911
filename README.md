# README #

AI PROJECT
Ćwiczenia projektowe

1.Sztuczny neuron – idea, budowa, model McCulloch-Pitts, perceptron (działanie, klasyfikacja z wykorzystaniem perceptronu), uczenie (perceptronu i dowolnego neuronu), zastosowanie, przykłady (proste układy funkcji logicznych)

- klasyfikacja do dwóch klas na podstawie dwóch danych - przypisanie do klasy 1 albo 0 

2.Sztuczna sieć neuronowa – architektura (sieć jedno- i wielo- warstwowa), uczenie (reguła delta, algorytm wstecznej propagacji błędu), zastosowanie, przykłady realizacji prostych układów logicznych, praktyczne uwagi uczenia sieci, problemy uczenia sieci

- wykorzystanie dla rozpoznawania znaków z bazy danych MNIST

3.Uczenie sieci z i bez nauczyciela, reguła Hebba, zastosowanie, przykłady.

4.Sieci Kohonena, uczenie (WTA), odwzorowanie cech istotnych (sieci samoorganizujące - SOMy), uczenie sieci SOM, przykłady, zastosowanie.

5.Sieci rekurencyjne (Hopfielda), uczenie, zastosowanie (pamięć asocjacyjna), przykłady.


Autor: Marcin Gol

Prowadzący: Grzegorz Górecki