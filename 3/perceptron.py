
from pylab import rand,norm
import math
import numpy as np
import matplotlib.pyplot as plt
import random
import time

def MSE(results, predictions): #Mean Square Error function - lowest error
  x = 0
  for result, prediction in zip(results, predictions):
    x += math.pow(prediction - result, 2)   # suming  squares of difference in values
  return x / len(results)

def MAPE(results, predictions): #Mean Absolute Percentage Error function - aka MAPD (forecasting method in statistics)
  x = 0
  for result, prediction in zip(results, predictions):
    if result == 0:
      print 'Couldn\'t complete MAPE method- division by 0'
      return 0
    x +=  (prediction - result) / prediction  # suming difference in values divided by result
  return (100 / len(results)) * x

class Perceptron:
 def __init__(self, inputs):
  """ perceptron initialization """
  self.w = np.random.random(inputs)
  self.learningRate = 0.01

 def countY(self, x):
  """ counts value of updated y for perceptron activation function """
  y = 0
  for weight, value in zip(self.w, x):
    y += value*weight
  y = (1 / 1 + math.e**(-y)) #sigmoid function
  return y

 def response(self,x):
  """ universal response for perceptron output """
  y = self.countY(x)
  if y >= 0:
   return 1
  else:
   return -1
    
 def response_n(self,x):
   """ universal response for perceptron output """
   y = self.countY(x)
   return y

 def updateWeights(self,x):
  """
   updates the weights status, w at time t+1 is
       w(t+1) = w(t) + constans*learningRate*x
   where d is desired output and r the perceptron response
   iterError is (d-r)
  """
  for i in range(min(len(self.w), len(x))):
    		self.w[i] = self.w[i] + 1.*self.learningRate*x[i] #no foregtterino
    		#self.w[i] = self.w[i]* + (1-self.learningRate)*1.*self.learningRate*x[i]*self.countY(x) #foregtterino
    		#self.w[i] = self.w[i]* + self.learningRate*self.countY(x)*x[i] #ojjerino
				
  #print 'weights:', self.w
  weights_x.append(self.w[0])
  weights_y.append(self.w[1])

 def train(self,data):
  """ 
   trains all the vector in data.
   Every vector in data must have three elements
  """
  learned = False
  iteration = 0

  while not learned:
    globalError = 0.0
    localError = 0.0
    #MSE and MAPE counting
    val = []
    pr = []
    for x in data: # for each samples
    #unsupervised learning
      r = self.response(x)
      #oji and noforgetterino with self.countY(x) - 1
      val.append(self.countY(x))
      #supervised learning
      r = self.response(x)
      if x[-1] != r: # if we have a wrong response
        iterError = x[-1] - r # desired response - actual response
        self.updateWeights(x)
      ### unsupervised learning
      pr.append(r)
      self.updateWeights(x)
      localError += 1 - abs(self.countY(x)- r)
    iteration += 1
    listError.append(localError)
    #measure MSE and MAPE
    mse_plot.append(MSE(val,pr))
    mape_plot.append(MAPE(val,pr))
    
    if MSE(val, pr) < 0.000001 or iteration >= 10000: # stop criteria
      if localError < 0.1:
        print 'Yay, we learned well'
      learned = True # stop learning


def generateData(n):
    """ 
    generates a 2D linearly separable dataset with n samples. 
    """
    xb = (rand(n)*2-1)/2
    yb = (rand(n)*2-1)/2
    inputs = []
    for i in range(len(xb)): #third is a label for supervised option
      inputs.append([xb[i],yb[i], random.randint(0, 1)])
    return inputs

# data for errors
listError = []
weights_x = []
weights_y = []

#for plotting erros
mse_plot = []
mape_plot = []

trainset = generateData(20) # train set generation
perceptron = Perceptron(2)   # perceptron instance

#measure time start
start = time.time()

perceptron.train(trainset)  # training

#measure time stop 
end = time.time()
print (end - start)
#testset = generateData(10)  # test set generation

#for sake of counting MSE and MAPE
values = []
predictions = []
for x in trainset:
  #temp = perceptron.countY(x)
  #temp = perceptron.w[0]*x[0] + perceptron.w[1]*x[1];
  temp = perceptron.countY(x)
  pred = perceptron.response(x)
  values.append(temp)
  #values.append(temp - 1) #forgetterino
  predictions.append(pred)

  if (temp - pred < 0.001):
  #if (temp - 1 - pred < 0.001):
    plt.plot(x[0],x[1],'ob')
  else:
    plt.plot(x[0],x[1],'or')

for x,y in zip(values, predictions):
      print x, ' should be ', y
print MAPE(values, predictions);
print MSE(values, predictions);

# plot of the separation line.
# The separation line classifies points
n = norm(perceptron.w)
ww = perceptron.w/n
ww1 = [ww[1],-ww[0]]
ww2 = [-ww[1],ww[0]]

plt.plot([ww1[0], ww2[0]],[ww1[1], ww2[1]],'--')
plt.show()

# plot chart of perceptron errors
# The bigger values, the bigger mistake
plt.plot(listError, '-')
plt.show()

# weights plot
plt.plot(weights_x, '-')
plt.plot(weights_y, '--')
plt.show()


# errors plot
plt.plot(mse_plot, '-')
plt.plot(mape_plot, '--')
plt.show()
