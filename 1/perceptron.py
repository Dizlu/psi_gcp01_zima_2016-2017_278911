
from pylab import rand,norm
import math
import numpy as np
import matplotlib.pyplot as plt
import random
import time

def MSE(results, predictions): #Mean Square Error function - lowest error
  x = 0
  for result, prediction in zip(results, predictions):
    x += math.pow(result - prediction, 2) # suming  squares of difference in values
  #print 'x is: ', x
  return x / len(results)

def MAPE(results, predictions): #Mean Absolute Percentage Error function - aka MAPD (forecasting method in statistics)
  x = 0
  for result, prediction in zip(results, predictions):
    if result == 0:
      print 'Couldn\'t complete MAPE method- division by 0'
      return 0
    x +=  (prediction - result) / prediction  # suming difference in values divided by result
  #print 'x is: ', x  
  return (100 / len(results)) * x

class Perceptron:
 def __init__(self, inputs):
  """ perceptron initialization """
  self.w = np.random.random(inputs)
  self.learningRate = 0.001

 def countY(self, x):
  """ counts value of updated y for perceptron activation function """
  y = 0
  for weight, value in zip(self.w, x):
    y += value*weight
  #y = (1 / 1 + math.e**(-y)) #sigmoid function
  return y

 def response(self,x):
  """ universal response for perceptron output """
  y = self.countY(x);
  if y >= 0:
   return 1
  else:
   return 0
    
 def response_n(self,x):
   """ universal response for perceptron output """
   y = self.countY(x);
   #print y
   if y == x[-1]:
    return 1
   else:
    return y

 def updateWeights(self,x,iterError):
  """
   updates the weights status, w at time t+1 is
       w(t+1) = w(t) + learningRate*(d-r)*x
   where d is desired output and r the perceptron response
   iterError is (d-r)
  """
  for i in range(min(len(self.w), len(x))):
    self.w[i] = self.w[i] + self.learningRate*iterError*x[i]
  print 'new weight: ', self.w

  weights_x.append(self.w[0])
  weights_y.append(self.w[1])

 def train(self,data):
  """ 
   trains all the vector in data.
   Every vector in data must have three elements,
   the third element (x[-1]) must be the label (desired output)
  """
  learned = False
  iteration = 0
  while not learned:
    localError = 0.0
    #MSE and MAPE counting
    val = []
    pr = []
    for x in data: # for each samples
      r = self.response(x)
      val.append(self.countY(x))
      pr.append(x[-1])
      print val
      print pr
      if x[-1] != r: # if we have a wrong response
        iterError = x[-1] - r # desired response - actual response
        print iterError
        self.updateWeights(x,iterError)
        localError += x[-1] - abs(self.countY(x) - r)
    iteration += 1
    listError.append(localError)
    mse_plot.append(MSE(val,pr))
    #mape_plot.append(MAPE(val,pr))

    if MSE(val, pr) < 0.0001 or iteration >= 10000: # stop criteria
      if localError < 0.1:
        print 'Yay, we learned well'
      learned = True # stop learning

def generateData(n):
    """ 
    generates a 2D linearly separable dataset with n samples. 
    The third element of the sample is the label
    """
    
    xb = (rand(n)*2-1)/2-0.5
    yb = (rand(n)*2-1)/2+0.5
    inputs = []
    for i in range(len(xb)):
        inputs.append([xb[i],yb[i], random.randint(0, 1)])
    return inputs
# data for errors
listError = []
weights_x = []
weights_y = []

#for plotting erros
mse_plot = []
mape_plot = []

trainset = generateData(6) # train set generation
perceptron = Perceptron(2)   # perceptron instance

#measure time start
start = time.time()

perceptron.train(trainset)  # training

#measure time stop 
end = time.time()
print (end - start)

testset = generateData(1)  # test set generation


#for sake of counting MSE and MAPE
values = []
predictions = []
for x in trainset:
  #temp = perceptron.countY(x)
  #temp = perceptron.w[0]*x[0] + perceptron.w[1]*x[1];
  temp = perceptron.response(x)
  pred = x[-1]
  #values.append(temp)
  values.append(temp) #forgetterino
  predictions.append(pred)

  #if (temp - pred < 0.001):
  if (temp - 1 - pred < 0.001):
    plt.plot(x[0],x[1],'ob')
  else:
    plt.plot(x[0],x[1],'or')

for x,y in zip(values, predictions):
      print x, ' should be ', y
#print MAPE(values, predictions);
print MSE(values, predictions);

# plot of the separation line.
# The separation line classifies points
n = norm(perceptron.w)
ww = perceptron.w/n
ww1 = [ww[1],-ww[0]]
ww2 = [-ww[1],ww[0]]

plt.plot([ww1[0], ww2[0]],[ww1[1], ww2[1]],'--')
plt.show()

# plot chart of perceptron errors
# The bigger values, the bigger mistake
plt.plot(listError, '-')
plt.show()

# weights plot
plt.plot(weights_x, '-')
plt.plot(weights_y, '--')
plt.show()


# errors plot
plt.plot(mse_plot, '-')
plt.plot(mape_plot, '--')
plt.show()